/////////////////////
//// INIT MODULE ////
/////////////////////
var InitModule = (function(Modernizr) {
  'use strict';


  ////////////////////////////
  //// VARIABLES PRIVADAS ////
  ////////////////////////////
  var vars = {},
    methods = {},
    viewToLoad = location.search.substring(1),
    mobileDetection,
    browserDetection;




  ////////////////////////////
  //// VARIABLES GLOBALES ////
  ////////////////////////////
  vars = {
    isMobile: false,
    firefox: false,
    ie9: false,
    ie10: false,
    ie11: false
  };






  //////////////////////////
  //// METODOS PRIVADOS ////
  //////////////////////////
  mobileDetection = function () {
    if (window.Detectizr.device.type !== 'desktop' || Modernizr.mq('(max-width: 1200px)')) {
      vars.isMobile = true;
    } else {
      vars.isMobile = false;
    }
  };

  browserDetection = function () {
    if (window.Detectizr.browser.name === 'ie') {
      if (window.Detectizr.browser.major === '9') {
        vars.ie9 = true;
      }
      if (window.Detectizr.browser.major === '10') {
        vars.ie10 = true;
      }
      if (window.Detectizr.browser.major === '11') {
        vars.ie11 = true;
      }
      if (window.Detectizr.browser.major < 9) {
        $('.update-browser').show(0);
        $('.main').hide(0);
      }
    }
  };






  //////////////////////////
  //// METODOS PUBLICOS ////
  //////////////////////////

  methods.ready = function() {

    //Imagenes upload
    $('.dropify').dropify();


    //Switch
    // var switchery = new Switchery(elem, { color: '#7c8bc7', jackColor: '#9decff' });

    var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));
    elems.forEach(function(html) {
      var switchery = new Switchery(html, { color: '#2BDBA6', jackColor: '#fff' });
    });


    //Efecto mantener label top siexiste texto
    $('input, textarea').blur(function(){
      if( $(this).val() == '' ){
        $(this).removeClass('has-value');
      }else{
        $(this).addClass('has-value');
      }
    });

    // Treeview
    $(function() {
      var defaultData = [
        {
          text: 'Parent 1',
          href: '#parent1',
          tags: ['4'],
          nodes: [
            {
              text: 'Child 1',
              href: '#child1',
              tags: ['2'],
              nodes: [
                {
                  text: 'Grandchild 1',
                  href: '#grandchild1',
                  tags: ['0']
                },
                {
                  text: 'Grandchild 2',
                  href: '#grandchild2',
                  tags: ['0']
                }
              ]
            },
            {
              text: 'Child 2',
              href: '#child2',
              tags: ['0']
            }
          ]
        },
        {
          text: 'Parent 2',
          href: '#parent2',
          tags: ['0']
        },
        {
          text: 'Parent 3',
          href: '#parent3',
           tags: ['0']
        },
        {
          text: 'Parent 4',
          href: '#parent4',
          tags: ['0']
        },
        {
          text: 'Parent 5',
          href: '#parent5'  ,
          tags: ['0']
        }
      ];
      var $checkableTree = $('#treeview-checkable').treeview({
        data: defaultData,
        showIcon: false,
        showCheckbox: true,
        onNodeChecked: function(event, node) {
          $('#checkable-output').prepend('<p>' + node.text + ' was checked</p>');
        },
        onNodeUnchecked: function (event, node) {
          $('#checkable-output').prepend('<p>' + node.text + ' was unchecked</p>');
        }
      });

      var findCheckableNodess = function() {
        return $checkableTree.treeview('search', [ $('#buscar_familias').val(), { ignoreCase: false, exactMatch: false } ]);
      };
      var checkableNodes = findCheckableNodess();

      // Check/uncheck/toggle nodes
      $('#buscar_familias').on('keyup', function (e) {
        checkableNodes = findCheckableNodess();
        $('.check-node').prop('disabled', !(checkableNodes.length >= 1));
      });
    });

    // $(document).on('click','.flat-butt-add',function(){
    //   $('#agregarMaterial tr:last').after("<tr><td>holhj</td></tr>");
    // });
    // Editar item en tabla
    $(document).on('click', '.table .ico.edit', function(){
      $(this).toggleClass('hidden');
      $(this).next().toggleClass('hidden');
      $(this).parents('tr').find('td:nth-child(3) .count-input').toggleClass('hidden').siblings().toggleClass('hidden');
    });

    $(document).on('click', '.table .ico.save', function(){
      $(this).toggleClass('hidden');
      $(this).prev().toggleClass('hidden');
      $(this).parents('tr').find('td:nth-child(3) .count-input').toggleClass('hidden').siblings().toggleClass('hidden');
    });

    //view mode table list and image
    $(document).on('click', '.btn-dinamic a', function(){
      $('.btn-dinamic a').removeClass('active');
      $(this).addClass('active');
      var view = $(this).attr('data-view');
      $('.view').addClass('hidden');
      $('#'+view).removeClass('hidden');
    });

    //Input TAg
    $('.tags').tagsInput();

    //Select2
    // $('select').select2({
    //     minimumResultsForSearch: -1
    // });

    //autofocus
    $(document).on('shown.bs.modal', function (e) {
        $('[autofocus]', e.target).focus();
    });

    //Search autocomplete
    // var countries = new Bloodhound({
    //     datumTokenizer: Bloodhound.tokenizers.whitespace,
    //     queryTokenizer: Bloodhound.tokenizers.whitespace,
    //     prefetch: 'https://twitter.github.io/typeahead.js/data/countries.json'
    //   });
    //
    var bestPictures = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.obj.whitespace('value'),
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        prefetch: 'https://twitter.github.io/typeahead.js/data/films/post_1960.json',
        remote: {
          url: 'https://twitter.github.io/typeahead.js/data/films/queries/%QUERY.json',
          wildcard: '%QUERY'
        }
      });

      $('#search-insumos').typeahead({
        hint: false,
        highlight: true,
        minLength: 3
        },
        {
          name: 'best-pictures',
          display: 'value',
          source: bestPictures,
          templates: {
            empty: [
              '<div class="empty-message" style="padding-left:10px;">',
                'No se encontro ningun registro',
              '</div>'
          ].join('\n'),
          suggestion: Handlebars.compile('<div><strong>{{value}}</strong> – {{year}}</div>')
        }
      });


      //Agregar efecto-linea material a inputs
      $('.form-group .form-control').each(function(i,v) {
        $(v).after('<div class="form-control-line"></div>');
      });

    //Succes Guardar
    function guardar(mensaje){
      mensaje = 'Se guardo correctamente';
      Lobibox.notify('success', {
          position: 'top right',
          msg: mensaje
      });
    };

    $( "#form_generales" ).validate( {
      rules: {
        nombres: "required",
        codigo: "required",
        descripcion_corta:"required",
        descripcion_larga:"required",
        product_manager:"required",
        familias:"required",
        agree: "required"
      },
      messages: {
        firstname: "Please enter your firstname",
        lastname: "Please enter your lastname",
        username: {
          required: "Please enter a username",
          minlength: "Your username must consist of at least 2 characters"
        },
        password: {
          required: "Please provide a password",
          minlength: "Your password must be at least 5 characters long"
        },
        confirm_password: {
          required: "Please provide a password",
          minlength: "Your password must be at least 5 characters long",
          equalTo: "Please enter the same password as above"
        },
        email: "Please enter a valid email address",
        agree: "Please accept our policy"
      },
      errorElement: "em",
      errorPlacement: function ( error, element ) {
        // Add the `help-block` class to the error element
        error.addClass( "help-block" );

        if ( element.prop( "type" ) === "checkbox" ) {
          error.insertAfter( element.parent( "label" ) );
        } else {
          error.insertAfter( element );
        }
      },
      highlight: function ( element, errorClass, validClass ) {
        $( element ).parents( ".col-sm-5" ).addClass( "has-error" ).removeClass( "has-success" );
      },
      unhighlight: function (element, errorClass, validClass) {
        $( element ).parents( ".col-sm-5" ).addClass( "has-success" ).removeClass( "has-error" );
      }
    });


    //Guardar
    $(document).on('click', '#btn_guardar', function(){
      guardar();
    });


    //icheck
    $('.icheck').iCheck({
      checkboxClass: 'icheckbox_square',
      radioClass: 'iradio_square',
      increaseArea: '20%' // optional
    });




    //Translation Jquery validation
    (function( factory ) {
    	if ( typeof define === "function" && define.amd ) {
    		define( ["jquery", "../jquery.validate"], factory );
    	} else if (typeof module === "object" && module.exports) {
    		module.exports = factory( require( "jquery" ) );
    	} else {
    		factory( jQuery );
    	}
    }(function( $ ) {

    /*
     * Translated default messages for the jQuery validation plugin.
     * Locale: ES (Spanish; Español)
     */
    $.extend( $.validator.messages, {
    	required: "Este campo es obligatorio.",
    	remote: "Por favor, rellena este campo.",
    	email: "Por favor, escribe una dirección de correo válida.",
    	url: "Por favor, escribe una URL válida.",
    	date: "Por favor, escribe una fecha válida.",
    	dateISO: "Por favor, escribe una fecha (ISO) válida.",
    	number: "Por favor, escribe un número válido.",
    	digits: "Por favor, escribe sólo dígitos.",
    	creditcard: "Por favor, escribe un número de tarjeta válido.",
    	equalTo: "Por favor, escribe el mismo valor de nuevo.",
    	extension: "Por favor, escribe un valor con una extensión aceptada.",
    	maxlength: $.validator.format( "Por favor, no escribas más de {0} caracteres." ),
    	minlength: $.validator.format( "Por favor, no escribas menos de {0} caracteres." ),
    	rangelength: $.validator.format( "Por favor, escribe un valor entre {0} y {1} caracteres." ),
    	range: $.validator.format( "Por favor, escribe un valor entre {0} y {1}." ),
    	max: $.validator.format( "Por favor, escribe un valor menor o igual a {0}." ),
    	min: $.validator.format( "Por favor, escribe un valor mayor o igual a {0}." ),
    	nifES: "Por favor, escribe un NIF válido.",
    	nieES: "Por favor, escribe un NIE válido.",
    	cifES: "Por favor, escribe un CIF válido."
    } );
    return $;
    }));

    //Input plus
   $(".incr-btn").on("click", function (e) {
       var $button = $(this);
       var oldValue = $button.parent().find('.quantity').val();
       $button.parent().find('.incr-btn[data-action="decrease"]').removeClass('inactive');
       if ($button.data('action') == "increase") {
           var newVal = parseFloat(oldValue) + 1;
       } else {
           // Don't allow decrementing below 1
           if (oldValue > 1) {
               var newVal = parseFloat(oldValue) - 1;
           } else {
               newVal = 1;
               $button.addClass('inactive');
           }
       }
       $button.parent().find('.quantity').val(newVal);
       e.preventDefault();
   });

   //Carusel
   $('.owl-carousel').owlCarousel({
      loop: true,
      margin: 10,
      dots: false,
      responsiveClass: true,
      responsive: {
        0: {
          items: 1,
          nav: true
        },
        900: {
          items: 2,
          nav: false
        },
        1000: {
          items: 3,
          nav: true,
          loop: false,
          margin: 20
        }
      }
    });


    //DETECTAR SI ES MOBILE
    mobileDetection();

    //DETECTAR NAVEGADORES
    browserDetection();


    //CARGA DE VISTAS (SOLO EN FRONTEND)
    if (viewToLoad.length > 0) {
      $('.main-content').load(viewToLoad + '.html', function() {
        //INICIAR
        methods.init();
      });
    } else {
      //INICIAR
      methods.init();
    }
  };


  methods.init = function() {
    //BOTONES
    methods.eventsList();

    //IMPEDIR EFECTO ROLLOVER EN MÓVILES
    if (vars.isMobile) {
      $('.hover').removeClass('hover');
    }
  };


  methods.eventsList = function() {
    //FUNCIONES QUE SE DEBEN EJECUTAR EN EL RESIZE
    $(window).on('resize', methods.resizeActions);
  };


  methods.resizeActions = function() {
    mobileDetection();

    browserDetection();
  };






  return {
    methods: methods,
    vars: vars
  };


})(Modernizr);





//CUANDO HA CARGADO EL DOM
$(document).ready(InitModule.methods.ready);
